﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public  class Student
    {
        public int Age { get; set; }
        public String  Name { get; set; }
        public int Sorce { get; set; }


        public Student(int age, String name, int sorce)
        {
            Age = age;
            Name = name;
            Sorce = sorce;

            Console.WriteLine("构造方法在此！");
            Console.WriteLine();
        }


        public Student(int age, string name) : this(age, name, 2)
        {


            Console.WriteLine("我就两个，嘻嘻嘻！");
            Console.WriteLine();
        }


        public void PrinStudentInfo()
        {
            Console.WriteLine("姓名：{0}，年龄：{1}，成绩：", Name, Age, Sorce);
            Console.WriteLine();
        }

        public Student (int age):this (age, "布兰登.英格拉姆", 1)
        {
            Console.WriteLine();
        }



        ~Student()
        {
            Console.WriteLine("这个是析构函数。。。。。。。。");

            Console.WriteLine( );
            Console.WriteLine();
        }



    }
}
