﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program2
    {
        private static  void  Main(String[] args)
        {
            Student student = new Student(19, "林鸿鑫", 100);

            student.PrinStudentInfo();

            Student student1 = new Student(19, "英格拉姆");

            student1.PrinStudentInfo();

            Student student2 = new Student(19);

            student2.PrinStudentInfo();
            
        }
    }
}
