﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public  class SumUntil
    {
        public int Sum(int a, int b)
        {
            return a + b;
            Console.WriteLine("这是整数型");
        }


        public double Sum(double a, double b)
        {
            return a + b;
            Console.WriteLine("这是双精度型");
        }

        public string Sum(string a, string b)
        {
            return a + b;
            Console.WriteLine("这是字符串型");
        }
    }
}
