﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class User
    {
        public User(string name, string password, string tel,string  sex)
        {
            this.Name = name;
            this.Password = password;
            this.Tel = tel;
            this.Sex = sex;
        }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Tel { get; set; }
        public string Sex { get; set; }
        public void PrintMsg()
        {
            Console.WriteLine("用户名：" + this.Name);
            Console.WriteLine("密  码：" + this.Password);
            Console.WriteLine("手机号：" + this.Tel);
            Console.WriteLine("性 别："+this.Sex);
        }
    }
}
