﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test7
{
    class Lipstick
    {
        public int id;
        public string name;
        public int price;

        public Lipstick()
        {
            Console.WriteLine();
        }

        public Lipstick(int id,string name,int price)
        {
            this.id = id;
            this.name = name;
            this.price = price;
        }
    }
}
