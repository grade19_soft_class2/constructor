﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp5
{
    public class Book
    {
        public Book(int id,string name,int price)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public void Book1()
        {
            Console.WriteLine("编号:{0} 书名:{1} 价格:{2}",Id,Name,Price);
        }
        ~Book()
        {
            Console.WriteLine("我的{0}哪里去了",Name);
        }
    }
}
