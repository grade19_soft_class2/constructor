﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC
{
    class Student
    {
        //类的访问修饰符有四种：internal and public
        //构造函数执行没有优先级，起查找作用，匹配到就执行，看两个构造函数是否向相同，是根据数据类型与顺序。
        public int Age { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }

        public Student(int age,string name,int score)
        {
            Age = age;
            Name = name;
            Score = score;
            Console.WriteLine("三个参数的构造方法");
            Console.WriteLine();
        }
        public  Student(int age,string name):this(age,name,0)
        {
            Age = age;
            Name = name;
        }

        public Student(int age):this(age,"第一时间",0)
        {
            
        }
        public Student() : this(20,"第一时间",0)
        {

        }

        public void PrintStudentInfo()
        {
            Console.WriteLine("姓名：{0},年龄：{1},成绩：{2}", Name, Age, Score);
            Console.WriteLine();
        }
        

        //public Student()
        //{
        //    Console.WriteLine("这是个构造函数，总觉得要做点什么......");

        //}
        //public Student(int age)
        //{
        //    Console.WriteLine("这是另一个析构函数，带一个参数的那种。");

        //}
        //public Student(int age,string name)
        //{

        //}
        //public Student(string name,int age)
        //{

        //}
        ~Student()
        {
            Console.WriteLine("这是一个析构函数，不知道做什么。");
            Console.WriteLine();

        }
       
    }
}
