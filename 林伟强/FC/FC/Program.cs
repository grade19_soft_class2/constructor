﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student(19, "小士墨疑", 99);  //将对象实例化， 构造函数是开始时做的事，析构函数是一件事结尾的时候要做的事情

            student.PrintStudentInfo();

            //构造函数与析构函数：欢迎光临，请问要吃些什么；析构收尾:欢迎光临，欢迎下次光临

            Student student1 = new Student(19, "小士墨染");

            student1.PrintStudentInfo();

            Student student2 = new Student();

            student2.PrintStudentInfo();



        }
    }
}
