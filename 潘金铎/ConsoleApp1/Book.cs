﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Book
    {
        private int Id { get; set; }
        private string Title { get; set; }
        private double Price { get; set; }
        public Book(int id,string title, double price)
        {
             Console.WriteLine("作者： 卢思浩");

            Id = id;
            Title = title;
            Price = price;
            Console.WriteLine("书的编码 {0}，书的名称《{1}》，书的价格{2}", Id , Title, Price);
        }
    }
}
