﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 构造函数
{
   public class Book1
    {
       
        private int Id { get; set; }
        private string Title { get; set; }
        private int Price { get; set; }

        public Book1(int id, string title, int price)
        {
            Id = id;
            Title = title;
            Price = price;
          
            Console.WriteLine("Book1中的构造函数被调用了");
           
        }

        //构造重载
        public Book1(int id, string title):this(id,title,20)
        {

        }
        public Book1():this(20,"马里奥救公主",20)
        {
        }

        public void Book()
        {
            Console.WriteLine("书本Id:{0} 书名:{1} 书本价格:{2}", Id, Title, Price);
        }
      

        //public int Id
        //{
        //    get { return _id; }
        //    set { _id = value; }
        //}
        //public string Title
        //{
        //    get { return _title; }
        //    set { _title = value; }
        //}
        //public int Price
        //{
        //    get { return _price; }
        //    set { _price = value; }
        //}

        //public void BookInformation(int Id, string Title, int Price)
        //{
        //    Console.WriteLine("书本Id:{0} 书名:{1} 书本价格:{2}", Id, Title, Price);
        //}

    }
}
