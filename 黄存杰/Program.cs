﻿using System;

namespace 构造函数
{
    class Program
    {
        static void Main(string[] args)
        {
            Book1 book = new Book1(20,"超级马里奥",20);
            book.Book();
            Book1 book1 = new Book1(20,"马里奥");
            book1.Book();
            Book1 book2 = new Book1();
            book2.Book();
        }
    }
}
