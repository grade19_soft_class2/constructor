﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class User
    {
        public User(string name, string password, string telephonenumber)
        {
            this.Name = name;
            this.Password = password;
            this.Tel = telephonenumber;
        }

        public string Name { get; set; }
        public string Password { get; set; }
        public string Tel { get; set; }

        public void PrintMsg()
        {
            Console.WriteLine("用户名：" + this.Name);
            Console.WriteLine("密  码：" + this.Password);
            Console.WriteLine("手机号：" + this.Tel);
        }
    }
}
