﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Hello
    {
        public Hello()
        {
            Console.WriteLine("Hello");
        }
        public Hello(string Name)
        {
            Console.WriteLine("我是"+Name);
        }
        public Hello(string Name, int Age)
        {
            Console.WriteLine("我是"+Name +"今年"+Age);
        }
        public Hello(string Name,int Age,string Sex)
        {
            Console.WriteLine("我是" +Name+ "今年"+Age+ "是个"+Sex);
        }

    }
}
