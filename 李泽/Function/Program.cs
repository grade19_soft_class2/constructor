﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    class Program
    {
        static void Main(string[] args)
        {
            SumResult sumResult = new SumResult();

            Console.WriteLine("两数的和"+sumResult.sum(10,20));
            Console.WriteLine("两个精准数字的和"+sumResult.sum(3.6,4.8));
            Console.WriteLine("两数的字符串的和"+sumResult.sum("小","李"));
            Console.WriteLine("数字为"+sumResult.sum(35));
           
        }
    }
}
