﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ConsoleApp3
{
     class Book
    {
        private int id;

        private string title;

        private int price;

        public Book(int id,string title,int price)
        {
           Id=id;
           Title=title;
           Price=price;
            }
        public int Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }

        public void BookInfo()
    {

            Console.WriteLine("书的编号:{0} 书名:{1} 书的价格:{2}",Id,Title,Price);
    }
        ~Book()
        {
            Console.WriteLine("{0}可好看啦！", Title);
        }
    }
    }

