﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public  class User
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Tel { get; set; }
        public User(string name,string password,string tel)
        {
            this.Name = name;
            this.Password = password;
            this.Tel = tel;
        }
        public void PrintMsg()
        {
            Console.WriteLine("用户名："+this.Name );
            Console.WriteLine("密  码："+this.Password );
            Console.WriteLine("手机号："+this.Tel );
        }
    }
}
