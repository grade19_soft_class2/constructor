﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }

        public Book(int id,string title, double price)
        {
            this.Id = id;
            this.Title = title;
            this.Price = price;
        }

        public void BookInfo()
        {
            Console.WriteLine("书籍编号：{0}\n书籍名称：{1}\n书籍价格：{2}\n",this.Id,this.Title,this.Price);
        }

        ~Book()
        {
            Console.WriteLine("{0}这本书是我的最爱啊！",Title);
        }
    }
}
