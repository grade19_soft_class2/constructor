﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text
{
    class Library
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }

        public Library(int id,string title,double price)//构造函数
        {
            Id = id;
            Title = title;
            Price = price;
        }
        
        public Library(int id,string title)//方法重载
        {
            Id = id;
            Title = title;
        }

        public Library(string title,double price) : this(3,title,price)
        {
            
        }

        public Library() : this(4,"HTML入门",6.5)
        {

        }

        public void Print()
        {
            Console.WriteLine("编号:{0}\n书名:《{1}》\n售价:{2}",Id,Title,Price);
        }

        ~Library()
        {
            Console.WriteLine("析构函数，释放内存");
        }
    }
}
