﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library(1,"C#从入门到入狱",32.8);
            library.Print();

            Console.WriteLine("\n");

            Library library2 = new Library(2, "Sql Server技术");
            library2.Print();

            Console.WriteLine("\n");

            Library library3 = new Library("Winform窗体应用",65);
            library3.Print();

            Console.WriteLine("\n");

            Library library4 = new Library();
            library4.Print();
            Console.ReadKey();
        }
    }
}
