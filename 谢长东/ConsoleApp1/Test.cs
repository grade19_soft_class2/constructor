﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Test
    {
        public string _name;
        public int _avg;


        //构造函数
        public Test()
        {
            Console.WriteLine("无参构造函数");
        }


        public Test(string name, int avg)
        {
            _name = name;
            _avg = avg;
            Console.WriteLine("构造函数");
        }


        //析构函数
        ~Test()
        {
            Console.WriteLine("析构函数");
        }


    }
}
