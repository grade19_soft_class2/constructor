﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book(10,"书");

            book.BookInfo();

            Book book2 = new Book(59);

            book2.BookInfo();
        }
        
    }
}
