﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Book
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int Price { get; set; }


        
        
        public Book(int id,string name,int price)
        {
            Id = id;
            Name = name;
            Price = price;
            Console.WriteLine("这是由三个参数构成的构造函数");
        }
        public Book(int id,string name):this(12,"格利佛游记",100)
        {
            Console.WriteLine();
        }
        public Book(int id) : this(99, "鲁滨逊漂流记", 190)
        {
            Console.WriteLine();
        }

        public void BookInfo()
        {
            Console.WriteLine("序号：{0} 书名：{1} 价格：{2}",Id,Name,Price);
        }
        ~Book()
        {
            Console.WriteLine("这是一个析构函数");
        }
    }
    
   
}
