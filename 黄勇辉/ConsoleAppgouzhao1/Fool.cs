﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppgouzhao1
{

     public class Fruit
    {
        public Fruit(int id, string name, double price)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }



        public void PrintMsg()
        {
            Console.WriteLine("水果的号码：{0}，\n水果的名字：{1},\n水果的价格:{2}元.", this.Id, this.Name, this.Price);
        }
    }
}
 
