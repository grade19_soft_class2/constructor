﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Constructor
{
    public class Function
    {
        private double number;
        public double Number
        {
            get { return number; }
            set
            {
                number = value;
                Console.WriteLine("Number={0}", number);
            }
        }
        public Function()
        {
            Console.WriteLine("嘻嘻，我的构造函数被触发了哦");
        }

        ~Function()
        {
            Console.WriteLine("哈哈，我的析构函数被触发了哦");
        }


    }
}
