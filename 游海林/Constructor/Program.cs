﻿using System;

namespace Constructor
{
    class Program
    {
        static void Main(string[] args)
        {
            Function _function;

            Console.WriteLine("Number有了，按任意键初始化Number");
            Console.ReadLine();

            _function = new Function() { Number = 12 };//初始化对象并赋一个值




            Console.WriteLine("按任意键，清除Number的值");
            Console.ReadLine();


            _function = null;//释放Number所占的内存


            if (_function == null)
                Console.WriteLine("Number==null");
            else
                Console.WriteLine("A的值为：{0}", _function.Number);
            Console.WriteLine("按任意键，回收Number");
            Console.ReadLine();


            GC.Collect();//启动垃圾回收机制，回收不需要的对象Number
            Console.ReadLine();
        }
    }
}
