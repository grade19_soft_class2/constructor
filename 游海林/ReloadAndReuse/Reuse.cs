﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReloadAndReuse
{
    class Reuse
    {
        private int hour;
        private int min;
        private int second;

        //5个构造函数
        public Reuse() : this(0, 0, 0) { }//this 复用构造函数初始器，原本0个参数值直接是默认的0

        public Reuse(int a) : this(a, 0, 0) { }//这里传入1个值，原则是是 到Get属性，但是这里复用了另一个构造函数（3个变量的构造函数）

        public Reuse(int a, int b) : this(a, b, 0) { }

        public Reuse(int a, int b, int c)
        {
            Settime(a, b, c);
        }

        public Reuse(Reuse reuse) : this(reuse.Hour, reuse.Min, reuse.Second) { }//对象作为构造函数的参数
        //1.这里是先读取Reuse的值
        //2.再根据构造函数赋值（复用构造函数）


        public void Settime(int hour, int min, int second)
        {
            Hour = hour;
            Min = min;
            Second = second;
        }


         
        public int Hour//属性是没有参数的
        {
            get { return hour; }
            private set { hour = value; }//这么写为了只能在构造函数里给初始化实例
        }


        public int Min
        {
            get { return min; }
            private set { min = value; }
        }

        public int Second
        {
            get { return second; }
            private set { second = value; }
        }


        public override string ToString()//重写ToString方法
        {
            return string.Format("hour :{0}  min :{1}  second : {2}", hour, min, second); ;
        }
    }
}
