﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReloadAndReuse
{
    class HeavyLoad
    {
        private int age; 
        private string name;
        private double height;

        public HeavyLoad()//不包含参数的构造函数
        {
            Console.WriteLine("The None !");
        }
        public HeavyLoad(int _age, string _name, double _height)
        {
            age = _age;
            name = _name;
            height = _height;
            Console.WriteLine("The First constructor !");
        }


        public HeavyLoad(string _name, int _age, double _height)
        {
            age = _age;
            name = _name;
            height = _height;
            Console.WriteLine("The Second constructor !");
        }

        public override string ToString()//对于对象的ToString方法必须重写
        {
            return string.Format(age + " " + name + " " + height); 
        }

    
}
}
