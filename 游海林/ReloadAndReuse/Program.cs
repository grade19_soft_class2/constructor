﻿
using System;

namespace ReloadAndReuse
{
    class Program
    {
        static void Main(string[] args)
        {
            HeavyLoad heavyLoad = new HeavyLoad(18, "xiaoyou", 180.00);//调用第一个构造函数

            Console.WriteLine(heavyLoad);//这里隐式ToString()方法
            HeavyLoad heavyLoad1 = new HeavyLoad("xiaoyou", 15, 180.00);//调用第二个构造函数
            Console.WriteLine(heavyLoad1);

            HeavyLoad heavyLoad2 = new HeavyLoad();//默认是隐式有一个不带参数的构造函数，但是如果自定义构造函数，不加参数会报错，所以要显式一个无参数构造函数
            Console.WriteLine(heavyLoad2);
            Console.WriteLine();


            Reuse reuse = new Reuse();
            D(reuse);

            Reuse reuse1 = new Reuse(10, 10, 10);
            D(reuse1);

            Reuse reuse2 = new Reuse(20, 20);
            D(reuse2);

            Reuse reuse3 = new Reuse(reuse2);//调用对象作为构造函数的参数
            D(reuse3);
        }

        static void D<T>(T reuse) 
        {
            Console.WriteLine(reuse);
        
    }
    }
}
