﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallingClassMember
{
    public class User
    {
        public User(int id,string name,int price)
        {
            Console.WriteLine("图书价格");
            Id = id;
            Name = name;
            Price = price;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }


        public void Num()
        {
            Console.WriteLine("图书编号:{0}  图书名称:{1}  图书价格:{2}", this.Id, this.Name, this.Price);
        }

        public void Nums(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public void Numss()
        {
            Console.WriteLine("图书编号:{0} 图书名称:{1}",Id,Name );
        }

        public int Sum(int a, int b)
        {
            return a + b;
        }
        public string Sum(string a,string b)
        {
            return a + b;
        }
        public double Sum(double a, double b)
        {
            return a + b;
        }

    }
}
