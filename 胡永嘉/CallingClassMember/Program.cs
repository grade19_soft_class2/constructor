﻿using System;

namespace CallingClassMember
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User(1, "白雪公主", 52);
            user.Num();

            user.Nums(2, "三只小猪");
            user.Numss();

            Console.WriteLine(user.Sum(26,26));
         
            Console.WriteLine(user.Sum(3.1,2.1));

            Console.WriteLine(user.Sum("小胖","不胖"));
        }
    }
}
