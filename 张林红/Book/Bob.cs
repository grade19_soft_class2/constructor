﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book
{
    class Bob
    {
        public static void Main(string[] args)
        {
            Program pr = new Program();
            pr.PeopleInfo(1, "Bob", "A1");
            pr.Student();
            Program pro = new Program();
            pro.PeopleInfo(2, "Alice", "B2");
            pro.Student();
        }
    }
}
