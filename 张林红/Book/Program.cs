﻿using System;
using System.Net.Cache;

namespace Book
{
    class Program
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string  Seat { set; get; }
        //构造函数
        public void Student()
        {
            Console.WriteLine("这个人的ID是：{0}，这个人的名字是：{1}，这个人的座位是：{2}；", this.Id, this.Name, this.Seat);
        }
        //带参数的构造函数
        public void Student(int age)
        {
            Console.WriteLine("嘤");
        }
        public void PeopleInfo(int id,string name,String seat)
        {
            this.Id = id;
            this.Name = name;
            Seat = seat;
        }
        //析构函数
        ~Program()
        {
            Console.WriteLine("这个人：{0}，嘤嘤嘤", Name);
        } 
        public Program()
        {
        }
        public Program(int id, string name, string seat)
        {
            Id = id;
            Name = name;
            Seat = seat;

        }
        public Program (int id,string name) : this(id, name, "0")
        {
            Id = id;
            Name = name;
        }

       
    }
}
