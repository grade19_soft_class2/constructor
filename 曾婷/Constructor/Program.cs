﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    class Program
    {
        static void Main(string[] args)
        {
            Girl girl = new Girl("曾婷",19,3147332144);
            girl.GirlInfo();
            Console.WriteLine("两数之和为：" + girl.Sum(5, 9));//整数求和
            Console.WriteLine("两数之和为：" + girl.Sum(2.4, 7.5));//小数求和
            Console.WriteLine("1-10与5相乘之和为：" + girl.Sum(10));//一到十与五相乘的和
            Girl girl1 = new Girl();
            Girl girl2 = new Girl("曾婷");
            Girl girl3 = new Girl("曾婷", 19);

        }
    }
}
