﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    class Girl
    {
        private string name;
        private int age;
        private uint QQ;

        public Girl(string name, int age, uint QQ)
        {
            this.name = name;
            this.age = age;
            this.QQ = QQ;
        }
        
        public void GirlInfo()
        {
            Console.WriteLine("女孩姓名:"+this.name);
            Console.WriteLine("女孩年龄:"+this.age);
            Console.WriteLine("女孩qq:"+this.QQ);
        }
        //方法重载
        public int Sum(int a, int b)
        {
            return a + b;
        }
        public double Sum(double a, double b)
        {
            return a + b;
        }
        public string Sum(string a, string b)
        {
            return a + b;
        }
        public int Sum(int a)
        {
            int sum = 0;
            for (int i = 1; i <= 10; i++)
            {
                sum += i * 5;
            }
            return sum;
          }
        //构造重载
        public  Girl()
        {
           Console.WriteLine("Hello!");
        }
        public Girl(string name)
        {
            Console.WriteLine("Hello"+name);
        }
        public Girl(string name,int age)
        {
            Console.WriteLine("Hello"+name+","+age);
        }
        //析构函数
        ~Girl()
        {
            Console.WriteLine("我听说{0}长得可漂亮啦",name);
        }
    }
}
