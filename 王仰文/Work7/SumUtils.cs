﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work7
{
    public class SumUtils
    {

        //方法重载

        
        public int Sum(int a,int b)
        {
            Console.WriteLine("调用了整形加法函数");
            return a + b;
        }

        public double Sum(double a, double b)
        {
            Console.WriteLine("调用了双精度浮点型加法函数");
            return a + b;
        }

        public string Sum(string a, string b)
        {
            Console.WriteLine("调用了字符串连接函数");
            return a + b;
        }

    }
}
