﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work7
{
    class Program
    {
        static void Main(string[] args)
        {   
            //构造函数的调用
            Student student = new Student(20,"天眼神兔",85);
            student.ShowStudentInfo();
            Console.WriteLine();


            //重载构造函数的调用
            Student student1 = new Student("神犬小七",999);
            student1.ShowStudentInfo();
            Console.WriteLine();

            
            //复用构造函数的调用
            Student student2 = new Student();
            student2.ShowStudentInfo();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();




            //重载方法的调用

            SumUtils sumUtils = new SumUtils();

            Console.WriteLine(sumUtils.Sum(5, 2));

            Console.WriteLine(sumUtils.Sum(56.9, 89.6666));

            Console.WriteLine(sumUtils.Sum("C#", "方法重载"));

        }
    }
}
