﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work7
{
    public class Student
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }

        public Student(int age, string name, int score)
        {
            this.Age = age;
            this.Name = name;
            this.Score = score;
            Console.WriteLine("这是个含三个参数的构造函数");
        }

        public Student(string name,int score)
        {
            Name = name;
            Score = score;
            Console.WriteLine("这是个重载构造函数");
        }

        public Student() : this(15, "修仙大能",99)
        {
            Console.WriteLine("这是个复用构造函数");
        }

        
        public void ShowStudentInfo()
        {
            Console.WriteLine("姓名:{0} 年龄:{1} 分数:{2}", Name,Age,Score);
        }


        ~Student()
        {
            Console.WriteLine();
            Console.WriteLine("这是个析构函数，程序结束，收！！！");
        }

    }
}
