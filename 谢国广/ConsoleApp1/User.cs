﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class User
    {
        public User (string name,string password,string tel)
        {
            this.Name = name;
            this.Password =  password;
            this.Tel = tel;
        }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Tel { get; set; }

        public void PrintMsg()
        {
            Console.WriteLine("用户名:"+this.Name );
            Console.WriteLine("密码:"+this.Password);
            Console.WriteLine("电话号码:"+Tel);
        }
    }
}
