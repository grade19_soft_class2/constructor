﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library(1, "《墨菲定律》", 4500);
            library.LibraryInfo();
            
            Console.WriteLine(library.sum(3,4));
            Console.WriteLine(library.sum(4.6,5.2));
            Console.WriteLine(library.sum("白雪公主","七个小矮人"));
           
        }
    }
}
