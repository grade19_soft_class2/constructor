﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace ConsoleApp1
{
    public class Mango
    {
       public Mango(int id,string name,int price)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public void Mango1()
        {
            Console.WriteLine("果龄:{0} 名称:{1} 价格:{2}",Id ,Name ,Price  );
        }

        ~Mango()
        {
            Console.WriteLine("我的{0}被谁吃了",Name );
        }
    }

}
