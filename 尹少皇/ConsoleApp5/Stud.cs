﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Stud
    {
        public string Num { get; set; } //学号
        public string Name { get; set; }//姓名
        public Stud()
        {
            ;
        }


        public Stud(string num, string name)
        {
            this.Num = num;
            this.Name = name;
        }

        public void Say()
        {
            Console.WriteLine("Hello,我的学号是{0}，名字{1}", this.Num, this.Name);
        }
    }
}
