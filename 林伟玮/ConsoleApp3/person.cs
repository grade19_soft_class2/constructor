﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public enum Gender
    {
        男,
        女
    }
    class person
    {
        public string name;
        public int age;
        public Gender gender;
        
        public person()
        {

        }

        public person(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public person (string name,int age,Gender gender ):this(name,age )
        {

            this.gender = gender;
        }

        public void ShoveInfor ()
        {
            Console.WriteLine("名字叫{0}，今年{1}，性别为{2}", name, age, gender);
            
        }
    }
}
