﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class person
    {
        public string name;
        public double price;
        public string color;


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public double Price
        {
          get { return price;}
          set { price = value; }
        }

        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        public void bazzr()
        {
            Console.WriteLine("物品名字：" + name);
            Console.WriteLine("物品价格：" + price);
            Console.WriteLine("物品颜色：" + color);
        }
    }
 
}
