﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    public  class Class1
    {
        public string Major { get; set; }

        public int Number { get; set; }

        public int People { get; set; }
        public Class1(string major,int number,int people)
        {
            Major = major;
            Number = number;
            People = people;
        }
        public Class1(string major, int number) :this (major ,number ,0)
        { 

        }
        public void PrintClassInfo()
        {
            Console.WriteLine("专业:{0},班级编号:{1},人数:{2}",Major,Number ,People);
        }
    }
}
