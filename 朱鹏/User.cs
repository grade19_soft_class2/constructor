﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    class User
    {
        public User(string name, string character, string tel)
        {
            this.Name = name;
            this.Character = character;
            this.Tel = tel;
        }
        public string Name { get; set; }
        public string Character { get; set; }
        public string Tel { get; set; }
        public void PrintMsg()
        {
            Console.WriteLine("用户名：" + this.Name);
            Console.WriteLine("性格：" + this.Character);
            Console.WriteLine("账号：" + this.Tel);
        }
    }
}
