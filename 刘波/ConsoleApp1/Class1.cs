﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student
    {
        public Student(int Age, string Name )
        {
            Console.WriteLine("调用了学生信息");

            _Age = Age;
            _Name = Name;
        }

        public int _Age { get; set; }

        public string _Name { get; set; }

        public void PrintStudent()
        {
            Console.WriteLine("学生年龄：{0} 学生名字 ：{1}",_Age,_Name);
        }

    }
}
