﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student(1);
            Student student1 = new Student(1,"孙权",'男');
            student1.Text();
            Student.Speak(1,2);
            Student.Speak(1, 2, 3);
            Console.ReadLine();
        }
    }
}
