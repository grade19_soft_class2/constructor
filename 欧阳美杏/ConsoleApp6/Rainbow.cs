﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp6
{
    class Rainbow
    {
        private int id;
        private string title;
        private int price;
        public Rainbow(int id,string title,int price)
        {
            Id = id;
            Title = title;
            Price = price;

        }
        public int Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }

        public void RainbowInfo()
        {
            Console.WriteLine("编号:{0} 书名:{0} 价格:{2}", Id, Title, Price);
        }
        ~Rainbow()
        {
            Console.WriteLine("{0}彩虹彩虹", Title);
        }
    }
}
