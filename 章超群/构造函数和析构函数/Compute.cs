﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
   class Compute
    {
        public int Sum(int a,int b)
        {
            Console.WriteLine("欢迎调用整形");
            return a + b;
        }
        public double  Sum(double a,double b)
        {
            Console.WriteLine("欢迎使用浮点型");
            return a + b;   
        }
        public string Sum(string a,string b)
        {
            Console.WriteLine("欢迎使用字符串型");
            return a + b;
        }
    }
}
