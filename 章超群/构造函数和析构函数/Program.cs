﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Library library = new Library(); 

            //Library libr = new Library(18);

            Library library = new Library("成功", "文科楼对面的左边", 10000000 );

            library.PrintLibrary();

            Library library2 = new Library("知己", "闽西职业技术学院");

            library2.PrintLibrary();

            Library library3 = new Library();

            library3.PrintLibrary();

            Compute compute = new Compute();

            Console.WriteLine(compute.Sum(1, 2));

            Console.WriteLine(compute.Sum(1.0, 2.0));

            Console.WriteLine(compute.Sum("超", "群"));


        }
    }
}
