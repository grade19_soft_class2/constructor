﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    public class Library
    {
        //public Library()
        //{
        //    Console.WriteLine("这是没有参数的构造函数");
        //}
        //public Library(int age)
        //{
        //    Console.WriteLine("这是有一个参数的构造函数");
        //}
        public string Name { get; set; }
        public int Size { get; set; }
        public string Place { get; set; }

        public Library(string names,string place,int size)
        {
            Name = names;
            Size = size;
            Place = place;
            Console.WriteLine("这是三个参数的构造函数");
            Console.WriteLine();
        }
        public Library(string name,string place) : this(name, place,66)
        {

        }
        public Library() : this("深知", "江西省", 1000000000)
        {

        }

        public void PrintLibrary()
        {
            Console.WriteLine("{0}图书馆,坐落于:{1},{2}万平方米", Name,Place,Size);
        }

        ~Library()
        {   /*析构函数:对象被销毁时系统的最后一次调用*/
            Console.WriteLine("调用了析构函数");
            Console.WriteLine();
        }
    }
}
