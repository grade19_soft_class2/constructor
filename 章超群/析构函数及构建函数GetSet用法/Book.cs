﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Book
    {   
        public Book(string name,int id,double price)
        {
            this.name = name;
            this.id = id;
            this._price = price;
            Console.WriteLine("调用构造的函数 格式:访问修饰符 类名(参数){语句}");
        }
        
        private string name;
        private int id;
        private double _price;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }

        }
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public double Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }
        public void NameIdPrice()
        {
            Console.WriteLine("书的名称:《{0}》 书的序号:{1} 书的价格:{2}",this.name,this.id,this._price);
        }
        public void AgaginNameIdPrice(string name,int id,double price)/*(数据类型加名称)*/
        {
            this.name = name;
            this.id = id;
            _price = price;
            Console.WriteLine("书的名称:《{0}》 书的序号:{1} 书的价格:{2}",name,id,price);
        }
        ~Book()
        {
            Console.WriteLine("析构函数把《{0}》吃掉了 格式~类名(参数){语句}",this.name);
        }

    }
}
