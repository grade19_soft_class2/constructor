﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ConsoleApp2
{
    public class Static
    { 
        /*静态类成员一定是静态的*/
        public static int Size { get; set; }
        public static string Name { get; set; }
        public static string Weight { get; set; }
        public static void AppleBook()
        {
            Console.WriteLine("书的大小:{0} 书的名称:{1} 书的重量:{2}",Size,Name,Weight);
        }
        public static void NewAppleBook(int size, string name, string weight)
        {
            Size = size;
            Name = name;
            Weight = weight;

        }
           
    }
}
