﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class SayWoof
    {
        public SayWoof()
        {
            Console.WriteLine("Woof!!!");
        }
        public SayWoof(string name)
        {
            Console.WriteLine("Woof!!!" + name);
        }
        public SayWoof(string name, int id)
        {
            Console.WriteLine("Woof!!!" + name + "，" + id);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            SayWoof say1 = new SayWoof();
            SayWoof say2 = new SayWoof("大黄");
            SayWoof say3 = new SayWoof("小黑", 520);
        }
    }
}
