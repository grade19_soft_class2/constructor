﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Compute
    {
        public int Add(int a, int b)
        {
            return a + b;
        }
        public double Add(double a, double b)
        {
            return a + b;
        }
        public string Add(string a, string b)
        {
            return a + b;
        }
        public int Add(int a)
        {
            int sum = 1;
            for (int i = 1; i <= a; i++)
            {
                sum *= i;
            }
            return sum;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Compute s = new Compute();

            Console.WriteLine("两个整数和：" + s.Add(3, 5));

            Console.WriteLine("两个小数和：" + s.Add(3.2, 5.6));

            Console.WriteLine("字符串连接：" + s.Add("C#", "方法重载"));

            Console.WriteLine("5的阶乘为：" + s.Add(5));
        }
    }
}
