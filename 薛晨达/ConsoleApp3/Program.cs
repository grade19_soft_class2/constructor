﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Person
    {
        public Person(string name, int age, string sex)
        {
            this.Name = name;
            this.Age = age;
            this.Sex = sex;
        }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public void PrintMsg()
        {
            Console.WriteLine("姓名：" + this.Name);
            Console.WriteLine("年龄：" + this.Age);
            Console.WriteLine("性别：" + this.Sex);
        }
        ~Person()
        {
            Console.WriteLine("调用了析构方法");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person("小王", 18, "男");
            person.PrintMsg();
        }
    }
}
